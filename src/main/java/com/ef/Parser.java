package com.ef;

import com.ef.model.Duration;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootApplication
public class Parser implements ApplicationRunner {

    private static final String ACCESS_LOG = "accesslog";
    private static final String DURATION = "duration";
    private static final String START_DATE = "startDate";
    private static final String THRESHOLD = "threshold";

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private Job job;

	public static void main(String[] args) {
		SpringApplication.run(Parser.class, args);
	}

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();

        for (String name : applicationArguments.getOptionNames()) {
            String value = applicationArguments.getOptionValues(name).get(0);

            switch (name) {
                case ACCESS_LOG:
                    if (StringUtils.isBlank(value)) {
                       throw new IllegalArgumentException(String.format("accessLog argument value[%s] is invalid. Must be a validate file path", value));
                    }
                    jobParametersBuilder.addString(ACCESS_LOG, value);
                    break;
                case START_DATE:
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss");
                    Date startDate = sdf.parse(value);
                    if (!value.equals(sdf.format(startDate))){
                        throw new IllegalArgumentException(String.format("startDate argument value[%s] is invalid. Must be a date 'yyyy-MM-dd.HH:mm:ss'", value));
                    }
                    jobParametersBuilder.addDate(START_DATE, startDate);
                    break;
                case DURATION:
                    String duration = value.toUpperCase();
                    if (Duration.valueOf(duration) == null) {
                        throw new IllegalArgumentException(String.format("duration argument value[%s] is invalid. Must be 'hourly' or 'daily'", value));
                    }
                    jobParametersBuilder.addString(DURATION, duration);
                    break;
                case THRESHOLD:
                    try {
                        long threshold = Long.parseLong(value);
                        jobParametersBuilder.addLong(THRESHOLD, threshold);
                    } catch (NumberFormatException e) {
                        throw new IllegalArgumentException(String.format("threshold argument value[%s] is invalid. Must be an integer", value));
                    }
                    break;
                default:
                    break;
            }
        }

        jobLauncher.run(job, jobParametersBuilder.toJobParameters());
    }

}
