package com.ef.batch;

import com.ef.model.Blocked;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BlockedRowMapper implements RowMapper<Blocked> {

    @Override
    public Blocked mapRow(ResultSet resultSet, int i) throws SQLException {
        Blocked blocked = new Blocked();
        blocked.setIp(resultSet.getString("ip"));
        blocked.setTotalRequests(resultSet.getLong("total_requests"));
        blocked.setComment("Exceeded threshold");
        return blocked;
    }

}
