package com.ef.batch;

import com.ef.model.Blocked;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.util.List;

public class BlockedItemWriter implements ItemWriter<Blocked> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BlockedItemWriter.class);

    @Autowired
    private EntityManager entityManager;

    @Override
    public void write(List<? extends Blocked> items) {
        for (Blocked blocked : items) {
            entityManager.persist(blocked);
            LOGGER.info(String.format("IP [%s] has [%s] requests", blocked.getIp(), blocked.getTotalRequests()));
        }
    }

}
