package com.ef.batch.configuration;

import com.ef.batch.BlockedItemWriter;
import com.ef.batch.BlockedRowMapper;
import com.ef.batch.LogFieldSetMapper;
import com.ef.model.Blocked;
import com.ef.model.Duration;
import com.ef.model.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(BatchConfiguration.class);

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    @StepScope
    public FlatFileItemReader<Log> flatFileItemReader(@Value("#{jobParameters['accesslog']}") String accessLog) {
        FlatFileItemReader<Log> reader = new FlatFileItemReader<>();
        reader.setResource(new FileSystemResource(accessLog));
        reader.setLineMapper(lineMapper());
        return reader;
    }

    @Bean
    public LineMapper<Log> lineMapper() {
        DefaultLineMapper<Log> lineMapper = new DefaultLineMapper<>();

        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setDelimiter("|");
        lineTokenizer.setStrict(false);
        lineTokenizer.setNames(new String[]{"date", "ip", "request", "status", "userAgent"});

        BeanWrapperFieldSetMapper<Log> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(Log.class);

        lineMapper.setLineTokenizer(lineTokenizer);
        lineMapper.setFieldSetMapper(fieldSetMapper());

        return lineMapper;
    }

    @Bean
    public FieldSetMapper<Log> fieldSetMapper() {
        return new LogFieldSetMapper();
    }

    @Bean
    @StepScope
    public JdbcBatchItemWriter<Log> jdbcBatchItemWriter(@Autowired DataSource dataSource) {
        JdbcBatchItemWriter<Log> writer = new JdbcBatchItemWriter<>();
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());
        writer.setDataSource(dataSource);
        writer.setSql("insert into log (date, ip, request, status, user_agent) values (:date, :ip, :request, :status, :userAgent)");
        return writer;
    }

    @Bean
    @StepScope
    public JdbcCursorItemReader<Blocked> jdbcCursorItemReader(
            @Autowired DataSource dataSource,
            @Value("#{jobParameters['duration']}") String duration,
            @Value("#{jobParameters['threshold']}") Long threshold,
            @Value("#{jobParameters['startDate']}") Date startDate) {

        JdbcCursorItemReader<Blocked> reader = new JdbcCursorItemReader<>();
        reader.setPreparedStatementSetter(preparedStatement -> {

            Instant instant = startDate.toInstant();
            Date endDate = null;

            switch (Duration.valueOf(duration)) {
                case HOURLY:
                    endDate = Date.from(instant.plus(1, ChronoUnit.HOURS));
                    break;
                case DAILY:
                    endDate = Date.from(instant.plus(1, ChronoUnit.DAYS));
                    break;
                default:
                    break;
            }

            preparedStatement.setDate(1, new java.sql.Date(startDate.getTime()));
            preparedStatement.setDate(2, new java.sql.Date(endDate.getTime()));
            preparedStatement.setLong(3, threshold);

        });
        reader.setSql("select ip, count(ip) as total_requests from log where date between ? and ? group by (ip) having total_requests > ?");
        reader.setDataSource(dataSource);
        reader.setRowMapper(rowMapper());
        return reader;
    }

    @Bean
    public RowMapper<Blocked> rowMapper() {
        return new BlockedRowMapper();
    }

    @Bean
    @StepScope
    public BlockedItemWriter blockedItemWriter() {
        return new BlockedItemWriter();
    }

    @Bean
    public Job job(@Qualifier("stepOne") Step stepOne, @Qualifier("stepTwo") Step stepTwo) {
        return jobBuilderFactory.get("job")
                .incrementer(new RunIdIncrementer())
                .flow(stepOne)
                .next(stepTwo)
                .end()
                .build();
    }

    @Bean("stepOne")
    public Step stepOne(@Autowired FlatFileItemReader<Log> flatFileItemReader, @Autowired JdbcBatchItemWriter<Log> jdbcBatchItemWriter) {
        return stepBuilderFactory.get("step-file-to-database")
                .<Log, Log> chunk(10)
                .reader(flatFileItemReader)
                .writer(jdbcBatchItemWriter)
                .build();
    }

    @Bean("stepTwo")
    public Step stepTwo(@Autowired JdbcCursorItemReader<Blocked> jdbcCursorItemReader, @Autowired BlockedItemWriter blockedItemWriter) {
        return stepBuilderFactory.get("step-ip-to-blocked")
                .<Blocked, Blocked> chunk(10)
                .reader(jdbcCursorItemReader)
                .writer(blockedItemWriter)
                .build();
    }

}
