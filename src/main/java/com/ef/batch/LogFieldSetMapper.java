package com.ef.batch;

import com.ef.model.Log;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

public class LogFieldSetMapper implements FieldSetMapper<Log> {

    @Override
    public Log mapFieldSet(FieldSet fieldSet) {
        Log log = new Log();
        log.setDate(fieldSet.readDate("date"));
        log.setIp(fieldSet.readString("ip"));
        log.setRequest(fieldSet.readString("request"));
        log.setStatus(fieldSet.readInt("status"));
        log.setUserAgent(fieldSet.readString("userAgent"));
        return log;
    }

}
