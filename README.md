# Parser

This project is an WalletHub challenge for the selective process. Its objective is parser an accesslog file to the database and filter IPs by daily or hourly threshold.

* [Java_MySQL_Test_Instructions.txt](https://bitbucket.org/leonardo_goncalves/parser/src/master/Java_MySQL_Test_Instructions.txt?fileviewer=file-view-default) - Instructions for implementation.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

To run this project you will need to install:

* Java 8 or superior
* Apache Maven 3.0.0 or superior
* MySQL 5.6.0. or superior

### Installing

Clone this repository into your local enviroment.

```
git clone https://leonardo_goncalves@bitbucket.org/leonardo_goncalves/parser.git
```

### Setting up Database

Install an instance of MySQL database and run the following scripts.

* [schema-mysql.sql](https://bitbucket.org/leonardo_goncalves/parser/src/master/schema-mysql.sql?fileviewer=file-view-default) - Create database schema.
* [query.sql](https://bitbucket.org/leonardo_goncalves/parser/src/master/query.sql?fileviewer=file-view-default) - Challenge queries.

**Attention**
If your database is installed in another host, you can set up the correct connection url through the property **spring.datasource.url** in [application.properties](https://bitbucket.org/leonardo_goncalves/parser/src/master/src/main/resources/application.yml?fileviewer=file-view-default#application.yml-10)

### Running

For the next commands you will need to have Maven in your system environment variable. To run the project, perform the following command:

```
mvn spring-boot:run --accessLog=/path/to/file --startDate=2017-01-01.15:00:00 --duration=hourly --threshold=200
```

## Deployment

For deployment, perform the following command:

```
mvn package
```

It will package the application at **target\parser.jar**.
The generated artifact can be deployed anywhere.
**Don't forget to set up the database for production environment, see this in the Setting up Database section.**
To run the project from the generated artifact, perform the following command:
```
java -jar parser.jar --accessLog=/path/to/file --startDate=2017-01-01.15:00:00 --duration=hourly --threshold=200
```

## Built With

* [Spring Boot](https://projects.spring.io/spring-boot/) - Stand-alone Spring applications
* [Spring Batch](https://projects.spring.io/spring-batch/) - Reusable functions that are essential in processing large volumes of records
* [Maven](https://maven.apache.org/) - Project management
* [MySQL](https://www.mysql.com) - Open source database

## Versioning

* [Git Flow](https://www.atlassian.com/git/tutorials/comparing-workflows#gitflow-workflow) - For development workflow.
* [SemVer](http://semver.org/) - For the versions available.

## Author

**Leonardo Costa Gonçalves**

* [Bitbucket](https://bitbucket.org/leonardo_goncalves/)
* [Linkedin](https://www.linkedin.com/in/leonardocg/)
* [leonardocg.12@gmail.com](mailto:leonardocg.12@gmail.com)